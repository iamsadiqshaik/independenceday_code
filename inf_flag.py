import turtle
#import winsound
win=turtle.Screen()
win.setup(width=800,height=600)
win.bgcolor('black')

stand=turtle.Turtle()
stand.color('white')
stand.shape('square')
stand.penup()
stand.setposition(-100,-280)
stand.pendown()
stand.goto(-100,280)

flag=turtle.Turtle()
flag.color('white')
flag.penup()
flag.setposition(-100,270)
flag.pendown()

length=400
width=80

def rect(color):
    flag.fillcolor(color)
    flag.begin_fill()
    flag.forward(length)
    flag.right(90)
    flag.forward(width)
    flag.right(90)
    flag.forward(length)
    flag.right(180)
    flag.end_fill()

rect('orange')
rect('white')
rect('green')
flag.hideturtle()

wheel=turtle.Turtle()
wheel.color('blue')
wheel.penup()
wheel.width(2)
wheel.goto(100,110)
wheel.pendown()
wheel.circle(40)
wheel.penup()
wheel.goto(100,150)
wheel.pendown()
for i in range (24):
    wheel.forward(41)
    wheel.backward(41)
    wheel.right(13.8)

text=turtle.Turtle()
text.speed(2)
text.hideturtle()
def write(message,pos,color):
    x,y=pos
    text.color('white')
    text.penup()
    text.goto(x,y)
    text.pendown()
    style=('Courier',40,'italic')
    text.write('LOVE for Nation \nstay home stay safe',font=style)
write('Indians',(55,-100),'pink')
turtle.done()
